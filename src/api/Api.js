//import axios
import axios from 'axios'

const Api = axios.create({
    //set default endpoint API
    baseURL: 'http://hibah-umkm.cathaa.site/api'    
})

export default Api