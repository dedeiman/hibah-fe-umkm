<template>
    <div class="card shadow-sm mb-4 mt-2 mt-md-0">
        <div class="card-header bg-white border-0 py-3">
            <div class="d-flex justify-content-start align-items-center">
                <div class="user-image-initial d-flex align-items-center justify-content-center"
                    :style="{'background-image': 'url(' + user.avatar + ')'}">
                    <h4 v-if="!user.avatar">
                        {{ user.name ? user.name.charAt(0) : '' }}
                    </h4>
                </div>
                <div class="px-3">
                    <h5 class="mb-0">
                        {{ user.name }}
                    </h5>
                    <p class="mb-0">
                        {{ user.email }}
                    </p>
                </div>
                <div class="ml-auto d-block d-md-none">
                    <a href="#" class="extend-sidemenu" @click="Sidebar = !Sidebar">
                        <i class="fas fa-times" v-if="Sidebar"></i>
                        <i class="fas fa-bars" v-else></i>
                    </a>
                </div>
            </div>
        </div>
        <div :class="{'card-body sidebar-account p-4': true, 'show': Sidebar}">
            <router-link :to="{name: 'edit-profile'}" class="d-flex align-items-center sidebar-text">
                <img :src="EditProfile" class="show" width="18" />
                <img :src="EditProfileActive" class="mouse-hover" width="18" />
                <h6 class="px-3 mb-0">Edit Profile</h6>
            </router-link>
            <router-link :to="{name: 'settings-address'}" class="d-flex align-items-center sidebar-text">
                <img :src="AddressSettings" class="show" width="18" />
                <img :src="AddressSettingsActive" class="mouse-hover" width="18" />
                <h6 class="px-3 mb-0">Pengaturan Alamat</h6>
            </router-link>
            <router-link :to="{name: 'settings-password'}" class="d-flex align-items-center sidebar-text">
                <img :src="PasswordSettings" class="show" width="18" />
                <img :src="PasswordSettingsActive" class="mouse-hover" width="18" />
                <h6 class="px-3 mb-0">Pengaturan Password</h6>
            </router-link>
            <router-link :to="{name: 'wishlist'}" class="d-flex align-items-center sidebar-text">
                <img :src="Wishlist" class="show" width="18" />
                <img :src="WishlistActive" class="mouse-hover" width="18" />
                <h6 class="px-3 mb-0">Wishlist</h6>
            </router-link>
            <router-link :to="{name: 'pay'}" class="d-flex align-items-center sidebar-text">
                <img :src="Transaction" class="show" width="18" />
                <img :src="TransactionActive" class="mouse-hover" width="18" />
                <h6 class="px-3 mb-0">Pembelian</h6>
            </router-link>
            <router-link :to="{name: 'refund'}" class="d-flex align-items-center sidebar-text">
                <img :src="Money" class="show" width="18" />
                <img :src="MoneyActive" class="mouse-hover" width="18" />
                <h6 class="px-3 mb-0">Refund</h6>
            </router-link>
        </div>
    </div>
</template>

<script>
    import EditProfile from '../assets/icons/edit-profile.svg';
    import AddressSettings from '../assets/icons/address-settings.svg';
    import PasswordSettings from '../assets/icons/password-settings.svg';
    import Wishlist from '../assets/icons/wishlist.svg';
    import Transaction from '../assets/icons/transaction.svg';
    import Money from '../assets/icons/money.svg';

    import EditProfileActive from '../assets/icons/edit-profile-active.svg';
    import AddressSettingsActive from '../assets/icons/address-settings-active.svg';
    import PasswordSettingsActive from '../assets/icons/password-settings-active.svg';
    import WishlistActive from '../assets/icons/wishlist-active.svg';
    import TransactionActive from '../assets/icons/transaction-active.svg';
    import MoneyActive from '../assets/icons/money-blue.svg';

    export default {
        setup() {
            return {
                EditProfile,
                AddressSettings,
                PasswordSettings,
                Wishlist,
                Transaction,
                Money,
                EditProfileActive,
                AddressSettingsActive,
                PasswordSettingsActive,
                WishlistActive,
                TransactionActive,
                MoneyActive,
            }
        },
        computed: {
            user() {
                return this.$store.getters['auth/currentUser']
            }
        },
        data() {
            return {
                Sidebar: false
            }
        }
    }
</script>

<style scoped>
    .user-image-initial {
        background-color: #FFFFFF;
        border: 1px solid var(--blue);
        text-align: center;
        width: 50px;
        height: 50px;
        border-radius: 100%;
        flex: 50px 0 0;
        background-size: cover;
        background-position: center;
    }

    .user-image-initial h4 {
        font-weight: bold;
        font-size: 19px;
        line-height: 26px;
        color: var(--blue);
        margin-bottom: 0;
    }

    .sidebar-account {
        height: 70vh;
        overflow-y: scroll;
    }

    .sidebar-text {
        text-decoration: none;
        margin-bottom: 20px;
    }

    .show {
        transition: 0.5s;
    }

    .mouse-hover {
        display: none;
    }

    .sidebar-text h6 {
        font-weight: 600;
        font-size: 12px;
        line-height: 16px;
        color: #404040;
        text-decoration: none;
        transition: 0.5s;
    }

    .sidebar-text:hover h6 {
        font-weight: 600;
        font-size: 12px;
        line-height: 16px;
        color: var(--blue);
        text-decoration: none;
    }

    .sidebar-text:hover .show {
        display: none;
    }

    .sidebar-text:hover .mouse-hover {
        display: block;
    }
</style>