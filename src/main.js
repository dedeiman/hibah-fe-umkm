import { createApp } from 'vue'
import App from './App.vue'

//import router
import router from './router'

//import store vuex
import store from './store'

//import dynamic layout


import AdminLayout from './layouts/AdminLayout.vue';
import CustomerLayout from './layouts/CustomerLayout.vue';
import BlankLayout from './layouts/BlankLayout.vue';

const app = createApp(App)

//dynamic layout
app.component('admin-layout', AdminLayout);
app.component('customer-layout', CustomerLayout);
app.component('default-layout', BlankLayout);

//gunakan router di vue js dengan plugin "use"
app.use(router)

//gunakan store di vue js dengan plugin "use"
app.use(store)

//define mixins for global function
app.mixin({

    methods: {

        //money thousands
        moneyFormat(number) {
            let reverse = number.toString().split('').reverse().join(''),
            thousands   = reverse.match(/\d{1,3}/g)
            thousands   = thousands.join('.').split('').reverse().join('')
            return thousands
        },

        //calculate discount
        calculateDiscount(product) {
            if(product.discount_type == 'nominal') {
                return product.price - product.discount
            }else{
                return product.price - (product.price *(product.discount)/100)
            }
        }

    }
})

app.mount('#app')
