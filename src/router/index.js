//import vue router
import { createRouter, createWebHistory } from 'vue-router'

//import store vuex
import store from '@/store'


//define a routes
const routes = [
  //main pages
  {
    path: '/register',
    name: 'register',
    meta: {
      layout: 'customer',
      requiresAuth: false
    },
    component: () => import('@/views/auth/Register.vue'),
  },
  {
    path: '/login',
    name: 'login',
    meta: {
      layout: 'customer',
      requiresAuth: false
    },
    component: () => import('@/views/auth/Login.vue'),
    props: true,
  },
  {
    path: '/forgot',
    name: 'forgot',
    meta: {
      layout: 'customer',
      requiresAuth: false
    },
    component: () => import('@/views/auth/ForgotPassword.vue')
  },
  {
    path: '',
    name: 'home',
    meta: {
      layout: 'customer',
      requiresAuth: false
    },
    component: () => import('../views/home/Index.vue')
  },
  {
    path: '/product',
    name: 'product',
    meta: {
      layout: 'customer',
      requiresAuth: false
    },
    component: () => import('../views/product/Index.vue')
  },
  {
    path: '/categories',
    name: 'categories',
    meta: {
      layout: 'customer',
      requiresAuth: false
    },
    component: () => import('../views/category/Index.vue')
  },
  {
    path: '/cart',
    name: 'cart',
    meta: {
      layout: 'customer',
      requiresAuth: false
    },
    component: () => import('../views/cart/Index.vue'),
  },
  {
    path: '/checkout',
    name: 'checkout',
    meta: {
      layout: 'customer',
      requiresAuth: false
    },
    component: () => import('../views/checkout/Index.vue'),
  },
  {
    path: '/payment',
    name: 'payment',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/payment/Index.vue'),
  },
  {
    path: '/payment/:slug',
    name: 'payment-choose',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/payment/PaymentChoose.vue'),
    props: true,
  },
  {
    path: '/order',
    name: 'order',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/order/Index.vue'),
  },
  {
    path: '/about',
    name: 'about',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/about/Index.vue'),
  },
  {
    path: '/contact',
    name: 'contact',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/contact/Index.vue'),
  },
  {
    path: '/blog',
    name: 'blog',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/blog/Index.vue'),
  },
  {
    path: '/blog/:slug',
    name: 'blog-show',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/blog/Show.vue'),
    props: true,
  },
  {
    path: '/term-and-condition',
    name: 'term-and-condition',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/term/Index.vue'),
  },
  {
    path: '/faq',
    name: 'faq',
    meta: {
      layout: 'customer',
      requiresAuth: true
    },
    component: () => import('../views/faq/Index.vue'),
  },
  //.main pages

  //admin
  {
    path: '/admin',
    meta: {
      layout: 'admin',
    },
    component: () => import('../views/admin/Index.vue'),
    children: [
      {
        path: '',
        name: 'admin-dashboard',
        component: () => import('../views/admin/dashboard/Index.vue')
      },
      {
        path: 'orders',
        name: 'admin-orders',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/orders/Index.vue'),
      },
      {
        path: 'orders-create',
        name: 'admin-orders-create',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/orders/Create.vue'),
      },
      {
        path: 'orders-detail/:id',
        name: 'admin-orders-detail',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/orders/Detail.vue'),
        props: true,
      },
      {
        path: 'slider',
        name: 'admin-slider',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/slider/Index.vue')
      },
      {
        path: 'slider-create',
        name: 'admin-slider-create',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/slider/Create.vue')
      },
      {
        path: 'about-us',
        name: 'admin-about',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/about/Index.vue'),
      },
      {
        path: 'product-create',
        name: 'admin-product-create',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/product/Create.vue'),
      },
      {
        path: 'product-list',
        name: 'admin-product-list',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/product/Index.vue'),
      },
      {
        path: 'product-category',
        name: 'admin-product-category',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/product/Category.vue'),
      },
      {
        path: 'product-category-create',
        name: 'admin-product-category-create',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/product/CategoryCreate.vue'),
      },
      {
        path: 'product-detail/:id',
        name: 'admin-product-detail',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/product/Detail.vue'),
        props: true,
      },
      {
        path: 'marketing-list',
        name: 'admin-marketing-list',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/marketing/Index.vue'),
      },
      {
        path: 'testimoni',
        name: 'admin-testimoni',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/testimoni/Index.vue'),
      },
      {
        path: 'testimoni-create',
        name: 'admin-testimoni-create',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/testimoni/Create.vue'),
      },
      {
        path: 'cash-book',
        name: 'admin-cash-book',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/book/Index.vue'),
      },
      {
        path: 'cash-book-create',
        name: 'admin-cash-book-create',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/book/Create.vue'),
      },
      {
        path: 'article',
        name: 'admin-article',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/article/Index.vue'),
      },
      {
        path: 'article-create',
        name: 'admin-article-create',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/article/Create.vue'),
      },
      {
        path: 'article-detail/:id',
        name: 'admin-article-detail',
        meta: {
          layout: 'admin'
        },
        component: () => import('../views/admin/article/Detail.vue'),
        props: true,
      },
    ]
  },
  
  //.admin
  { 
    path: '/:pathMatch(.*)*', 
    component: () => import('../components/404.vue')
  },
]

//create router
const router = createRouter({
    history: createWebHistory(),
    routes // <-- routes
})

//define route for handle authentication
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
      //cek nilai dari getters isLoggedIn di module auth
    if (store.getters['auth/isLoggedIn']) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router