//import global API
import Api from '../../api/Api'

const host = window.location.host;

console.log(host)

const auth = {

    //set namespace true
    namespaced: true,

    //state
    state: {

        //state untuk token, pakai localStorage, untuk menyimpan informasi tentang token JWT
        token: localStorage.getItem('token') || '',

        //state user, pakai localStorage, untuk menyimpan data user yang sedang login
        user: JSON.parse(localStorage.getItem('user')) || {},

        //
        domain: 'example.net'

    },

    //mutations
    mutations: {

        //update state token dan state user dari hasil response
        AUTH_SUCCESS(state, token, user) {
            state.token = token // <-- assign state token dengan response token
            state.user = user // <-- assign state user dengan response data user
        },

        //update state user dari hasil response register / login
        GET_USER(state, user) {
            state.user = user // <-- assign state user dengan response data user
        },

        //fungsi logout
        AUTH_LOGOUT(state) {
            state.token = '' // <-- set state token ke empty
            state.user = {} // <-- set state user ke empty array
        },

        UPDATE_PROFILE(state, user) {
            state.user.name = user.name
            state.user.avatar = user.avatar
        }

    },

    //actions
    actions: {

        //action register
        register({getters}, user) {
            var url = ''

            if(user.register_as == 'customer') {

                url = '/customer/register'

            }else if(user.register_as == 'reseller') {
                
                url = '/reseller/register'

            }else{  

                url = '/admin/register'
                
            }

            //define callback promise
            return new Promise((resolve, reject) => {

                var domain = getters.currentDomain

                //send data ke server
                Api.post(url, {

                        //data yang dikirim ke serve untuk proses register
                        name: user.name,
                        email: user.email,
                        phone_number: user.phone_number,
                        password: user.password,
                        password_confirmation: user.password_confirmation

                    }, {
                        headers: {
                            'X-DOMAIN-MEMBER': domain,
                        }
                    })

                    .then(response => {

                        //resolve ke component dengan hasil response
                        resolve(response)

                    }).catch(error => {

                        //reject ke component dengan hasil response
                        reject(error.response.data)

                    })

            })
        },

        //action getUserProfile
        getUserProfile({commit, getters}) {

            const domain = getters.currentDomain

            //ambil data token dari localStorage
            const token = localStorage.getItem('token')

            Api.defaults.headers.common['Authorization'] = "Bearer " + token
                
            Api.get('/customer/profile', {
                headers: {
                    'X-DOMAIN-MEMBER': domain,
                }
            })
            .then((res) => {
                var data = res.data.data
                
                commit('UPDATE_PROFILE', data.user)
            })
        },

        //action logout
        logout({commit}) {

            //define callback promise
            return new Promise((resolve) => {

                //commit ke mutation AUTH_LOGOUT
                commit('AUTH_LOGOUT')

                //remove value dari localStorage
                localStorage.removeItem('token')
                localStorage.removeItem('user')

                /**
                 * commit ke module cart, untuk set mutation dan state cart menjadi kosong
                 */
                commit('cart/CLEAR_CART', 0, {
                    root: true
                })

                //delete header axios
                delete Api.defaults.headers.common['Authorization']
                delete Api.defaults.headers.common['X-DOMAIN-MEMBER']

                //return resolve ke component 
                resolve()

            })
        },

        //action login
        login({commit, getters, dispatch}, user) {

            var domain = getters.currentDomain
            var url = ''

            if(user.login_as == 'customer') {

                url = '/customer/login'

            }else if(user.login_as == 'reseller') {
                
                url = '/reseller/login'

            }else{  

                url = '/admin/login'
                
            }

            //define callback promise
            return new Promise((resolve, reject) => {

                Api.post(url, {

                        //data yang dikirim ke server
                        email: user.email,
                        password: user.password,

                    }, {
                        headers: {
                            'X-DOMAIN-MEMBER': domain,
                        }
                    })

                    .then(response => {

                        //define variable dengan isi hasil response dari server
                        const token = response.data.data.token
                        var data = response.data.data.user

                        data.domain = domain
                        data.login_as = user.login_as

                        //set localStorage untuk menyimpan token dan data user
                        localStorage.setItem('token', token)
                        localStorage.setItem('user', JSON.stringify(data))

                        //set default header axios dengan token
                        Api.defaults.headers.common['Authorization'] = "Bearer " + token
                        Api.defaults.headers.common['X-DOMAIN-MEMBER'] = data.domain

                        //commit auth success ke mutation
                        commit('AUTH_SUCCESS', token, user)

                        //commit get user ke mutation
                        commit('GET_USER', data)

                        if(user.login_as == 'admin') {
                            return resolve(response)
                        }

                        //refresh cart dari module cart
                        dispatch(
                            'cart/refreshCart', 
                            {

                            },
                            {
                                root: true
                            }
                        )

                        //resolve ke component dengan hasil response
                        resolve(response)

                    }).catch(error => {

                        //jika gagal, remove localStorage dengan key token
                        localStorage.removeItem('token')

                        //reject ke component dengan hasil response
                        reject(error.response.data)

                    })

            })

        },

        loginGoogle() {

            return new Promise((resolve, reject) => {
                Api.get('/customer/google/login')

                    .then(response => {
                        console.log(response)

                        // document.write(response.data)
                        
                        var popup = window.open("","wildebeast","scrollbars=1,resizable=1")

                        var html = response.data

                        popup.document.open()
                        popup.document.write(html)
                        popup.document.close()

                        resolve(response)

                    }).catch(error => {

                        //jika gagal, remove localStorage dengan key token
                        localStorage.removeItem('token')

                        //reject ke component dengan hasil response
                        reject(error.response.data)

                    })
            })
        }

    },

    //getters
    getters: {

        //get current user
        currentUser(state) {
            return state.user // <-- return dengan data user
        },
        
        //get current user
        currentDomain(state) {
            return state.domain // <-- return dengan data domain
        },

        //loggedIn
        isLoggedIn() {
            var token = '$2y$10$OVFv2tjJ9wFpnNABl7tbm.2y//IxMnZTeag12HcYfmcBAZH.AFpJq'
            return token // return dengan data token
        },

    }

}

export default auth